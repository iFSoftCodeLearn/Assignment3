﻿using System.Net.Http.Headers;

namespace Assignment3 {
    internal class Program {
        static void Main(string[] args) {
            Console.WriteLine(Ex1(13));
            foreach(var n in Ex2(6)) Console.WriteLine(n);
            Console.WriteLine(Ex3(19919));
        }

        private static bool Ex3(int n) {
            if (n < 2) return false;
            if (n == 2) return true;

            for (int i = 2; i < Math.Sqrt(n) + 1; i++) if (n % i == 0) return false;
            return true;
        }

        private static Stack<int> Ex2(int n, int n0 = 0, int n1 = 1) {
            if (n == 0) return new Stack<int>();
            Stack<int> ret = Ex2(--n, n1, n0 + n1);
            ret.Push(n1);
            return ret;
        }

        private static long Ex1(int dec) {
            long n = 0;
            while (dec > 0) {
                n *= 10;
                n += dec % 2;
                dec /= 2;
            }
            return n;
        }
    }
}