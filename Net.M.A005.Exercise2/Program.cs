﻿namespace Net.M.A005.Exercise2 {
    internal class Program {
        static void Main(string[] args) {
            PrintFibonacci(9);
        }

        private static void PrintFibonacci(int v) { foreach (var n in Ex2(v)) Console.WriteLine(n); }

        private static Stack<int> Ex2(int n, int n0 = 0, int n1 = 1) {
            if (n == 0) return new Stack<int>();
            Stack<int> ret = Ex2(--n, n1, n0 + n1);
            ret.Push(n1);
            return ret;
        }
    }
}