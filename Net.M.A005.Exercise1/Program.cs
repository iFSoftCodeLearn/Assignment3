﻿namespace Net.M.A005.Exercise1 {
    internal class Program {
        static void Main(string[] args) {
            Console.WriteLine(ConvertToBinary(13));
        }

        private static long ConvertToBinary(int dec) {
            long n = 0;
            while (dec > 0) {
                n *= 10;
                n += dec % 2;
                dec /= 2;
            }
            return n;
        }
    }
}