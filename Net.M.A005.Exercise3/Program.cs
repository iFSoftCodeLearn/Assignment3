﻿namespace Net.M.A005.Exercise3 {
    internal class Program {
        static void Main(string[] args) {
            Console.WriteLine(CheckPrimeNumber(19919));
        }

        private static bool CheckPrimeNumber(int n) {
            if (n < 2) return false;
            if (n == 2) return true;

            for (int i = 2; i < Math.Sqrt(n) + 1; i++) if (n % i == 0) return false;
            return true;
        }
    }
}